import React from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  Button,
} from 'react-native';

const signin = () => {
  return (
    <SafeAreaView style={style.container}>
      <View style={[style.top, {backgroundColor: '#F4F4F4'}]}>
        <Image style={style.imageStyle} source={require('./logo-marlin.png')} />
        <Text style={[style.textStyle, {paddingTop: 20, paddingBottom: 30}]}>
          Please Sign In to Continue
        </Text>
        <TextInput
          style={style.input}
          placeholder="Username"
          keyboardType="words"
        />
        <TextInput
          secureTextEntry={true}
          style={style.input}
          placeholder="password"
          keyboardType="words"
        />
        <TouchableOpacity style={style.Button}>
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              fontFamily: 'nunito',
              textAlign: 'center',
            }}>
            Sign In
          </Text>
        </TouchableOpacity>
        <Text style={{marginTop: 30, marginBottom: 20, fontSize: 11}}>
          Forgot Password
        </Text>
        <View
          style={{
            width: 310,
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 20,
          }}>
          <View
            style={{
              flex: 1,
              height: 1,
              backgroundColor: '#959595',
              marginHorizontal: 10,
              marginTop: 10,
              marginBottom: 10,
            }}
          />
          <Text style={{fontSize: 11}}>Login With</Text>
          <View
            style={{
              flex: 1,
              height: 1,
              backgroundColor: '#959595',
              marginHorizontal: 10,
              marginTop: 10,
              marginBottom: 10,
            }}
          />
        </View>
        <View
          style={{
            paddingTop: 20,
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Image
            source={require('./facebook.png')}
            style={{width: 46, height: 46, marginRight: 30}}
          />
          <Image
            source={require('./google.png')}
            style={{width: 48, height: 48, marginLeft: 30}}
          />
        </View>
        <Text style={{paddingTop: 70}}>App version 2.8.3</Text>
      </View>
      <View style={[style.bottom, {backgroundColor: '#FFFFFF'}]}>
        <Text style={{fontSize: 11, fontFamily: 'nunito', color: '#5B5B5B'}}>
          Alredy have account? <Text style={{fontWeight: 'bold'}}>Sign Up</Text>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
    flexDirection: 'column',
  },
  top: {
    flex: 1,
    width: 400,
    paddingTop: 60,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  bottom: {
    width: 400,
    height: 40,
    alignSelf: 'center',
  },
  imageStyle: {
    width: 190.15,
    height: 44.87,
  },
  textStyle: {
    fontSize: 18,
    fontFamily: 'nunito',
    alignItems: 'center',
    color: '#5B5B5B',
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    marginTop: 10,
    width: 300,
    height: 43,
  },
  Button: {
    backgroundColor: '#2E3283',
    borderRadius: 10,
    marginTop: 10,
    padding: 10,
    width: 300,
    height: 43,
  },
  bottom: {
    height: 45,
    width: 400,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default signin;
