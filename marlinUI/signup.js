import React from 'react';
import {
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  ScrollView,
  Button,
} from 'react-native';

const signup = () => {
  return (
    <SafeAreaView style={style.container}>
      <View style={[style.top, {backgroundColor: '#F4F4F4'}]}>
        <Image style={style.imageStyle} source={require('./logo-marlin.png')} />
        <Text style={[style.textStyle, {paddingTop: 20, paddingBottom: 30}]}>
          Create an Account
        </Text>
        <TextInput
          style={style.input}
          placeholder="name"
          keyboardType="words"
        />
        <TextInput
          style={style.input}
          placeholder="email"
          keyboardType="words"
        />
        <TextInput
          style={style.input}
          placeholder="phone"
          keyboardType="numeric"
        />
        <TextInput
          secureTextEntry={true}
          style={style.input}
          placeholder="password"
          keyboardType="words"
        />
        <TouchableOpacity style={style.Button}>
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              fontFamily: 'nunito',
              textAlign: 'center',
            }}>
            Sign Up
          </Text>
        </TouchableOpacity>
      </View>
      <View style={[style.bottom, {backgroundColor: '#FFFFFF'}]}>
        <Text style={{fontSize: 11, fontFamily: 'nunito', color: '#5B5B5B'}}>
          Alredy have account? <Text style={{fontWeight: 'bold'}}>Sign In</Text>
        </Text>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
    flexDirection: 'column',
  },
  top: {
    flex: 1,
    width: 400,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
  },
  bottom: {
    width: 400,
    height: 40,
    alignSelf: 'center',
  },
  imageStyle: {
    width: 190.15,
    height: 44.87,
  },
  textStyle: {
    fontSize: 18,
    fontFamily: 'nunito',
    alignItems: 'center',
    color: '#5B5B5B',
  },
  input: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 10,
    margin: 10,
    marginTop: 10,
    width: 300,
    height: 43,
  },
  Button: {
    backgroundColor: '#2E3283',
    borderRadius: 10,
    marginTop: 10,
    padding: 10,
    width: 300,
    height: 43,
  },
  bottom: {
    height: 45,
    width: 400,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});

export default signup;
